""" Decode binary RFC 8446 (TLS) encoded log messages """

import construct

#TODO check if we can factor out file_type and version


# The log root is defined by Trillian:
#
# struct {
#   uint64 tree_size;
#   opaque root_hash<0..128>;
#   uint64 timestamp_nanos;
#   uint64 revision;
#   opaque metadata<0..65535>;
# } LogRootV1;
#
# enum { v1(1), (65535)} Version;
# struct {
#   Version version;
#   select(version) {
#     case v1: LogRootV1;
#   }
# } LogRoot;


# In RFC 8446, the size of length fields depends on their maximum value
LogRootV1 = construct.Struct(
    "tree_size" / construct.Int64ub,
    "root_hash_len" / construct.Int8ub,
    construct.Check(construct.this.root_hash_len <= 128),
    "root_hash" / construct.Bytes(construct.this.root_hash_len),
    "timestamp_nanos" / construct.Int64ub,
    "revision" / construct.Int64ub,
    "metadata_len" / construct.Int16ub,
    "metadata" / construct.Bytes(construct.this.metadata_len),
)

LogRoot = construct.Struct(
    "version" / construct.Enum(construct.Int16ub, v1=1),
    "logRoot" / construct.Switch(construct.this.version, {"v1": LogRootV1},
                                 default=construct.Error),
    construct.Terminated,
)


# signed receipt constitutes inclusion promise

ReceiptV1 = construct.Struct(
    "file_type" / construct.Enum(construct.Int16ub, release=0,
                                 not_release=65535),
    "file_hash_len" / construct.Int16ub,
    "file_hash" / construct.Bytes(construct.this.file_hash_len),
    "timestamp_nanos" / construct.Int64ub,
)

Receipt = construct.Struct(
    "version" / construct.Enum(construct.Int16ub, v1=1),
    "receipt" / construct.Switch(construct.this.version, {"v1": ReceiptV1},
                                 default=construct.Error),
    construct.Terminated,
)


# tree leaf

LeafV1 = construct.Struct(
    "file_type" / construct.Enum(construct.Int16ub, release=0,
                                 not_release=65535),
    "file_hash_len" / construct.Int16ub,
    "file_hash" / construct.Bytes(construct.this.file_hash_len),
)

Leaf = construct.Struct(
    "version" / construct.Enum(construct.Int16ub, v1=1),
    "leaf" / construct.Switch(construct.this.version, {"v1": LeafV1},
                              default=construct.Error),
    construct.Terminated,
)


inner = construct.Struct(
    "value" / construct.Int8ub,
)
test = construct.Struct(
    "version" / construct.Enum(construct.Int16ub, v1=1),
    "inta" / construct.Switch(construct.this.version, {"v1": inner},
                              default=construct.Error),
    construct.Terminated,
)
