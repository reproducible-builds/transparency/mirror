""" Offer various log related actions on mirror objects.

/srv/mirrors/strans/
    dists/<sha2>
    pool/
        .promise        receipt
        .proof          inclusion proof, tree root

/srv/mirrors/debian/proofs/
    <sha2>.proof        inclusion proof, tree root
    cons.<tree_size>    consistency proof (if possible), tree root

"""

import asyncio
import hashlib
import json
import pathlib

import aiofiles
from systemd import journal

import log


class NotReady(Exception):
    """ The log is unable to present an inclusion proof """
    pass


class Blob():
    """ Represent a mirrored artefact """
    def _init__(self):
        """ Call async init afterwards """
        pass

    async def init(self, path, session, url, pkey_path):
        """ Async initialisation """
        self.session = session
        self.relative_path = path
        self.absolute_path = pathlib.Path('/srv/mirrors/debian/' + path)
        self.sha2 = await sha2(self.absolute_path)

        prefix = '/srv/mirrors/strans/'
        if path.startswith('pool/'): # pool objects don't change
            prefix += path
        else:
            prefix += 'dists/' + self.sha2

        self.promise_path = pathlib.Path(prefix + '.promise')
        self.proof_path = pathlib.Path(prefix + '.proof')

        self.release_proof_dir = pathlib.Path('/srv/mirrors/debian/proofs')
        if not self.release_proof_dir.exists():
            self.release_proof_dir.mkdir()

        self.leaf = log.Leaf(bytes.fromhex(self.sha2), self.is_release)
        self.url = url
        self.pkey = log.PublicKey(pkey_path)


    def __str__(self):
        """ Identify by path """
        return self.relative_path


    def log(self, *args):
        """ Log into journal """
        msg = " ".join(str(arg) for arg in args)
        journal.send(msg + ": " + str(self), SYSLOG_IDENTIFIER='submit')


    @property
    def is_release(self):
        """ Does this object represent a release file """
        if self.absolute_path.name == 'InRelease' and \
           self.relative_path.startswith('zzz-dists/'):
            return True
        return False


    async def submit(self):
        """ Submit into log if need be and indicate that fact """
        if self.proof_path.exists():
            self.log("not submitting")
            return False
        self.log('submitting')

        try:
            self.promise_path.parent.mkdir(parents=True)
        except FileExistsError:
            pass

        while True:
            async with aiofiles.open(str(self.absolute_path), 'rb') as blob:
                path = 'submit-release' if self.is_release else 'submit'
                resp = await self.session.post(self.url + path,
                                               data=await blob.read())

            if resp.status == 200:
                self.log('submitted')
                break
            self.log('submission failed', resp.status, await resp.text())
            resp.close()

        promise_dict = await resp.json()
        resp.close()

        # check signature, file hash and timestamp
        _ = log.Receipt.from_response(promise_dict, self.pkey, self.leaf)

        self.log('storing receipt', str(self.promise_path))
        async with aiofiles.open(str(self.promise_path), 'w') as promise:
            await promise.write(json.dumps(promise_dict, indent=4))
        return True


    async def _get_root(self):
        """ Retrieve the current tree root """
        resp = await self.session.get(self.url + 'get-root')
        root_dict = await resp.json()
        resp.close()
        return log.Root.from_response(root_dict, self.pkey)


    async def _get_incl(self, root=None):
        """ Retrieve and verify an inclusion proof """
        if root is None:
            root = await self._get_root()
        params = {'tree-size': root.tree_size,
                  'leaf-hash': self.leaf.b64}
        resp = await self.session.get(self.url + 'get-incl-proof',
                                      params=params)
        if not resp.status == 200:
            self.log('get-incl failed', resp.status, await resp.text())
            resp.close()
            raise NotReady

        incl_dict = await resp.json()
        resp.close()
        root.includes_from_response(self.leaf, incl_dict)
        return root.resp_dict, incl_dict


    async def proof(self):
        """ Try twice to retrieve an inclusion proof and store it on disk """
        self.log('retrieving proof')
        try:
            root_dict, incl_dict = await self._get_incl()
        except NotReady: # allow the log to take a while to include the leaf
            self.log('retrieving proof failed, will retry')
            await asyncio.sleep(10)
            root_dict, incl_dict = await self._get_incl()

        async with aiofiles.open(str(self.proof_path), 'w') as proof:
            await proof.write(json.dumps({**root_dict, **incl_dict}, indent=4))


    def _stored_root(self):
        """ Get latest root from the stored consistency proof chain, or None
        """
        def key(path):
            """ Use the tree size as sorting index """
            return int(path.name.split('.')[1])

        cons = sorted(self.release_proof_dir.glob('cons.*'), key=key)
        if cons:
            root_dict = json.load(cons[-1].open())
            root = log.Root.from_response(root_dict, self.pkey)
            assert root.tree_size == key(cons[-1])
            return root


    async def _get_cons(self, fst, snd):
        """ Given two tree roots, retrieve and verify a consistency proof.  The
        roots are not being verified.
        """
        cons_params = {'first': fst.tree_size,
                       'second': snd.tree_size}
        resp = await self.session.get(self.url + 'get-cons-proof',
                                      params=cons_params)
        cons_dict = await resp.json()
        resp.close()
        snd.appends_to_from_response(fst, cons_dict)
        return cons_dict


    async def store_cons(self, root):
        """ Store the given root, or, if a previous root exists, also retrieve
        and store a consistency proof
        """
        self.log("storing release incl proof")
        fst = self._stored_root()
        if fst: # normal case
            self.log("storing cons proof")
            cons_dict = await self._get_cons(fst, root)
            # store consistency proof with root
            root_dict = root.resp_dict.copy()
            root_dict['audit_path'] = cons_dict['audit_path']
            root_dict['informative_first'] = fst.tree_size
            root_dict['informative_second'] = root.tree_size
        else: # trust on first use
            root_dict = root.resp_dict
        # bootstrap by storing only a root
        # else: store root and consistency proof to previous stored root
        cons_path = self.release_proof_dir / ('cons.' + str(root.tree_size))
        async with aiofiles.open(str(cons_path), 'w') as proof:
            await proof.write(json.dumps(root_dict, indent=4))


    async def release_proof(self, root=None):
        """ Store consistency proof and inclusion proof to specified root.
        Only call this after all files have been submitted and included into
        the log
        """
        if root is None: # get consistency proof
            _ = [path.unlink() for path in # cleanup
                 self.release_proof_dir.glob('*.proof')]
            root = await self._get_root()
            await self.store_cons(root)

        # store inclusion proof to this specific root
        root_dict, incl_dict = await self._get_incl(root)
        incl_path = self.release_proof_dir / (self.sha2 + '.proof')
        async with aiofiles.open(str(incl_path), 'w') as proof:
            await proof.write(json.dumps({**root_dict, **incl_dict}, indent=4))
        return root


async def sha2(path):
    """ Return SHA2 hex digest for a file object """
    hasher = hashlib.sha256()
    async with aiofiles.open(str(path), 'rb') as opened:
        hasher.update(await opened.read())
    return hasher.hexdigest()
