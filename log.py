""" Interface to the TLS encoded log server responses.  Verify cryptographic
proofs and signatures.
"""

import base64
import hashlib
import time

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.serialization import load_pem_public_key

import tls


class VerificationError(Exception):
    """ Custom cryptographic verification failed """
    pass


class PublicKey():
    """ Log public key """
    def __init__(self, path):
        """ pathlib.Path to an ECDSA PEM encoded public key """
        pem = path.open('rb').read()
        self.pkey = load_pem_public_key(pem, backend=default_backend())

    def verify(self, blob, signature):
        """ Verify signature on bytes """
        # raises cryptography.exceptions.InvalidSignature
        self.pkey.verify(signature, blob, ec.ECDSA(hashes.SHA256()))


class Receipt():
    """ Inclusion promise issued by the log """
    @staticmethod
    def from_response(resp_dict, pkey, leaf):
        """ Given an strans log API response, parse the receipt.
        Validate:
            - the receipt signature
            - that it matches the expected leaf
            - that the timestamp looks plausible
        """
        # signature check
        blob = base64.b64decode(resp_dict['receipt'])
        signature = base64.b64decode(resp_dict['receipt_signature'])
        pkey.verify(blob, signature) # raises on error

        # After signature verification we dare to unmarshal
        receipt = Receipt(blob)

        # match to leaf
        if receipt.file_type != leaf.file_type:
            raise VerificationError("File type does not match",
                                    receipt.file_type,
                                    leaf.file_type)
        if receipt.file_hash != leaf.file_hash:
            raise VerificationError("File hash does not match")

        # time check
        time_ns = time.time() * 10**9 # time_ns() available starting 3.7
        if receipt.timestamp_nanos >= time_ns:
            raise VerificationError("Receipt is from the future")

        return receipt

    def __init__(self, blob):
        """ Keep the content of the promise """
        receipt = tls.Receipt.parse(blob)
        self.file_type = receipt.receipt.file_type
        self.file_hash = receipt.receipt.file_hash
        self.timestamp_nanos = receipt.receipt.timestamp_nanos
        self.tls = blob


class Leaf():
    """ Represent a log leaf """
    def __init__(self, file_hash, is_release):
        """ Compute leaf hash from file hash """
        file_type = 'release' if is_release else 'not_release'
        v1_content = {'file_type': file_type,
                      'file_hash_len': len(file_hash),
                      'file_hash': file_hash}
        leaf = tls.Leaf.build({'version': 'v1', 'leaf': v1_content})

        self.file_type = file_type
        self.file_hash = file_hash
        self.hash = hashlib.sha256(b'\x00' + leaf).digest()

    @property
    def b64(self):
        """ Return base64 encoded leaf hash """
        return base64.b64encode(self.hash).decode()


class Root():
    """ Represent a log root """
    @staticmethod
    def from_response(resp_dict, pkey):
        """ Create log root from strans log API response, verifying the log
        signature
        """
        blob = base64.b64decode(resp_dict['log_root'])
        signature = base64.b64decode(resp_dict['log_root_signature'])
        pkey.verify(blob, signature)
        return Root(tls.LogRoot.parse(blob), resp_dict)

    def __init__(self, root, resp_dict):
        """ Provide a tls.LogRoot.  Does not verify signatures. """
        self.tree_size = root.logRoot.tree_size
        self.hash = root.logRoot.root_hash
        self.resp_dict = {'log_root': resp_dict['log_root'],
                          'log_root_signature': resp_dict['log_root_signature']}

    @staticmethod
    def normalise(audit_path):
        """ base64 is not decoded to bytes automatically """
        return [base64.b64decode(elem) for elem in audit_path]

    @staticmethod
    def _lsb(num):
        """ Is the least significant bit set? """
        return num % 2 == 1

    @staticmethod
    def _rshift_incl(fn_, sn_):
        """ Shift both right until fn_ is odd or zero """
        while True:
            fn_, sn_ = fn_ >> 1, sn_ >> 1
            if Root._lsb(fn_) or fn_ == 0: # ./ RFC: logical or, not xor
                return fn_, sn_

    def includes_from_response(self, leaf, resp_dict):
        """ Given an strans log API response, validate an inclusion proof """
        audit_path = Root.normalise(resp_dict['audit_path'])
        leaf_index = resp_dict['leaf_index']
        self.includes(leaf, leaf_index, audit_path)

    def includes(self, leaf, leaf_index, audit_path):
        """ Validate inclusion proof """
        # follow RFC 6962-bis
        if leaf_index >= self.tree_size: # 1.
            raise VerificationError("size", self.tree_size, leaf_index,
                                    leaf.hash)

        fn_ = leaf_index # 2.
        sn_ = self.tree_size - 1
        r__ = leaf.hash # 3. the root candidate

        for p__ in audit_path: # 4. iterate through path items
            if sn_ == 0:
                raise VerificationError("sn zero", self.tree_size, leaf_index,
                                        leaf.hash)
            if Root._lsb(fn_) or fn_ == sn_: # we are right of path item
                r__ = sha2(b'\x01' + p__ + r__) # 4-1.
                if not Root._lsb(fn_): # 4-2.
                    fn_, sn_ = Root._rshift_incl(fn_, sn_)
            else: # we are left of path item
                r__ = sha2(b'\x01' + r__ + p__) # 4-1.
            fn_, sn_ = fn_ >> 1, sn_ >> 1
        if sn_ == 0 and r__ == self.hash: # 5.
            return
        raise VerificationError("ended", self.tree_size, leaf_index, leaf.hash)

    @staticmethod
    def _rshift_cons(fn_, sn_):
        """ Right shift fn and sn until lsb of fn is not set anymore """
        while Root._lsb(fn_):
            fn_, sn_ = fn_ >> 1, sn_ >> 1
        return fn_, sn_

    @staticmethod
    def _rshift_cons2(fn_, sn_):
        """ Right shift fn and sn until until either lsb of fn is set or
        fn == 0
        """
        while True:
            fn_, sn_ = fn_ >> 1, sn_ >> 1
            if Root._lsb(fn_) or fn_ == 0: # ./ RFC: logical, not xor
                break
        return fn_, sn_

    @staticmethod
    def _is_pow(num):
        """ Is the number a power of two? """
        return num & num - 1 == 0

    def appends_to_from_response(self, prev_root, resp_dict):
        """ Verify consistency proof, given an older root and an strans log API
        response
        """
        audit_path = Root.normalise(resp_dict['audit_path'])
        self.appends_to(prev_root, audit_path)

    def appends_to(self, prev_root, audit_path):
        """ Verify consistency proof """
        first, first_hash = prev_root.tree_size, prev_root.hash
        second, second_hash = self.tree_size, self.hash
        if not 0 < first < second:
            raise VerificationError("size", first, second)

        if Root._is_pow(first): # 1.
            audit_path.insert(0, first_hash)
        fn_, sn_ = first - 1, second - 1 # 2.
        fn_, sn_ = Root._rshift_cons(fn_, sn_) # 3.

        fr_ = sr_ = audit_path[0] # 4.

        for c__ in audit_path[1:]: # 5.
            if sn_ == 0:
                raise VerificationError("sn zero", first, second)

            if Root._lsb(fn_) or fn_ == sn_:
                fr_ = sha2(b'\x01' + c__ + fr_) # 5-1.
                sr_ = sha2(b'\x01' + c__ + sr_)
                if not Root._lsb(fn_): # 5-2.
                    fn_, sn_ = Root._rshift_cons2(fn_, sn_)
            else:
                sr_ = sha2(b'\x01' + sr_ + c__) # 5-1.
            fn_, sn_ = fn_ >> 1, sn_ >> 1

        if fr_ == first_hash and sr_ == second_hash and sn_ == 0: # 6.
            return
        raise VerificationError("ended", first, second)


def sha2(data):
    """ Compute sha2 on bytes, returning bytes """
    return hashlib.sha256(data).digest()
